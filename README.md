# FakeDLL

These libdl stub replaces the operating system functions dlopen(...)
and dlsym(...) functions. They are intended for cases where dynamic
shared object loaded at runtime are not permitted, or when the shared object
should be incorporated into the code to build just one single executable.
They allow with very few changes to keep the same software architecture.
