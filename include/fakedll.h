//************************************************************************
//  These libdl stub replaces the operating system functions dlopen(...)
//  and dlsym(...) functions. They are intended for cases where dynamic
//  shared object loaded at runtime are not permitted, or when the shared object
//  should be incorporated into the code to build just one single executable.
//  They allow with very few changes to keep the same software architecture.
//
//  All the symbols from the shared objects must be registered prior use, with
//  a call to the function fake_dll_lib_reg.
//************************************************************************
#ifndef FAKE_DLL_FAKEDLL_H_ALLREADY_INCLUDED
#define FAKE_DLL_FAKEDLL_H_ALLREADY_INCLUDED

#ifdef __cplusplus
extern "C"
{
#endif

void fake_dll_lib_reg    (void*, const char*, const char*);

int  fake_dll_lib_load   (void**, const char*);
int  fake_dll_lib_unload (void*);
int  fake_dll_smb_get    (void**, void*, const char*);

#ifdef __cplusplus
}
#endif

#endif  // FAKE_DLL_FAKEDLL_H_ALLREADY_INCLUDED
