//************************************************************************
//  These libdl stub replaces the operating system functions dlopen(...)
//  and dlsym(...) functions. They are intended for cases where dynamic
//  shared object loaded at runtime are not permitted, or when the shared object
//  should be incorporated into the code to build just one single executable.
//  They allow with very few changes to keep the same software architecture.
//
//  All the symbols from the shared objects must be registered prior use, with
//  a call to the function fake_dll_lib_reg.
//  Then function pointers will be retrieve with dlopen and dlsym calls.
//************************************************************************
#ifndef FAKE_DLL_WINDLL_H_ALLREADY_INCLUDED
#define FAKE_DLL_WINDLL_H_ALLREADY_INCLUDED

#include <windows.h>

#ifndef FAKE_DLL
#  define FAKE_DLL_LoadLibrary      LoadLibrary
#  define FAKE_DLL_FreeLibrary      FreeLibrary
#  define FAKE_DLL_GetProcAddress   GetProcAddress

#else

#ifdef __cplusplus
extern "C"
{
#endif
HMODULE WINAPI FAKE_DLL_LoadLibrary    (LPCSTR);
BOOL    WINAPI FAKE_DLL_FreeLibrary    (HMODULE);
FARPROC WINAPI FAKE_DLL_GetProcAddress (HMODULE, LPCSTR);
#ifdef __cplusplus
}
#endif

#endif   // FAKE_DLL

#endif  // FAKE_DLL_WINDLL_H_ALLREADY_INCLUDED
