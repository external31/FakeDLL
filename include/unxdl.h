//************************************************************************
//  These libdl stub replaces the operating system functions dlopen(...)
//  and dlsym(...) functions. They are intended for cases where dynamic
//  shared object loaded at runtime are not permitted, or when the shared object
//  should be incorporated into the code to build just one single executable.
//  They allow with very few changes to keep the same software architecture.
//
//  All the symbols from the shared objects must be registered prior use, with
//  a call to the function fake_dll_lib_reg.
//  Then function pointers will be retrieved with dlopen and dlsym calls.
//  
//  Caveat:
//      The functions take for granted that the lifetime of the char* is 
//      greater than the use of the functions. No copy is made from the
//      supplied strings.
//************************************************************************
#ifndef FAKE_DLL_UNXDL_H_ALLREADY_INCLUDED
#define FAKE_DLL_UNXDL_H_ALLREADY_INCLUDED

#ifndef FAKE_DLL
#  include <dlfcn.h>
#  define FAKE_DLL_dlopen       dlopen
#  define FAKE_DLL_dlclose      dlclose
#  define FAKE_DLL_dlsym        dlsym
#  define FAKE_DLL_dlerror      dlerror

#else

#ifndef RTLD_LAZY
#   define RTLD_LAZY         0x001   /* Lazy function call binding.  */
#endif
#ifndef RTLD_NOW
#   define RTLD_NOW          0x002   /* Immediate function call binding.  */
#endif
#ifndef RTLD_BINDING
#   define RTLD_BINDING_MASK 0x003   /* Mask of binding time value.  */
#endif

#ifdef __cplusplus
extern "C"
{
#endif

void *FAKE_DLL_dlopen (const char *library, int /*flag*/);
int   FAKE_DLL_dlclose(void *handle);
void *FAKE_DLL_dlsym  (void *handle, const char *name);
char *FAKE_DLL_dlerror();

#ifdef __cplusplus
}
#endif

#endif   // FAKE_DLL

#endif  // FAKE_DLL_UNXDL_H_ALLREADY_INCLUDED
