//************************************************************************
//  These libdl stub replaces the operating system functions dlopen(...)
//  and dlsym(...) functions. They are intended for cases where dynamic
//  shared object loaded at runtime are not permitted, or when the shared object
//  should be incorporated into the code to build just one single executable.
//  They allow with very few changes to keep the same software architecture.
//
//  All the symbols from the shared objects must be registered prior use, with
//  a call to the function fake_dll_lib_reg.
//  Then function pointers will be retrieve with dlopen and dlsym calls.
//************************************************************************
#include "fakedll.h"

#include <assert.h>
#include <algorithm>
#include <map>
#include <string>

typedef std::map<std::string, void*>         TTLibrary;     // One lib
typedef std::pair<int, TTLibrary>            TTLibraryPair; // One lib + counter
typedef std::map<std::string, TTLibraryPair> TTLibraries;   // All the libs
static TTLibraries libs;

//  Predicate to control if an handle is known
struct handle_finder
{
    handle_finder(void* v_) : v(v_) {}
    bool operator() (const TTLibraries::value_type& l) const
    {
        return &(l.second) == v;
    }
    void* v;
};

#ifdef __cplusplus
extern "C"
{
#endif

//************************************************************************
//  The function fake_dll_lib_reg will register a DLL/SO symbol with
//  the supplied parameters.
//
//  Input:
//      void *fn;                   Opaque function pointer
//      const char *symbol;         Symbol name
//      const char *library         Shared Library (DLL) name
//************************************************************************
void fake_dll_lib_reg(void *fn , const char *symbol, const char *library)
{
#ifdef _DEBUG
    assert(fn      != NULL);
    assert(symbol  != NULL);
    assert(library != NULL);
    assert(strlen(symbol)  > 0);
    assert(strlen(library) > 0);
#endif  // _DEBUG

    TTLibraries::iterator libPairI = libs.find(library);
    if (libPairI == libs.end())
    {
        libPairI = libs.insert( TTLibraries::value_type(library, TTLibraryPair()) ).first;
        libPairI->second.first = 0;
    }
    libPairI->second.second[symbol] = fn;
}

//************************************************************************
//  Return the handle corresponding to the library.
//
//Description:
//  The load_lib function returns the handle corresponding to the module
//  specified by library. If the module is already loaded, it is not loaded
//  again, but a reference count will be incremented. 
//  The value returned by load_lib may be used in subsequent calls to get_sym and 
//  unload_lib. On success, load_lib returns a zero (0) value. If an error occurs
//  during the operation, load_lib returns a non-zero error code.
//
//Return Values:
//    -1:   unknown library
//************************************************************************
int fake_dll_lib_load(void **handle, const char *library)
{
   int ret = 0;
   
   TTLibraries::iterator libsI = libs.find(library);
   if (libsI == libs.end())
   {
      handle = NULL;
      ret = -1;
   }
   else
   {
      libsI->second.first++;        // Increment library counter
      *handle = &(libsI->second);   // Return a pointer to the pair
   }

   return ret;
}

//************************************************************************
//  Closes and unloads a module loaded by the dlopen function.
//
//Description:
//  The dlclose function is used to remove access to a module loaded with
//  the dlopen function. In addition, access to dependent modules of the
//  module being unloaded is removed as well.
//  Upon successful completion, 0 (zero) is returned. If the object could not be 
//  closed, or if handle does not refer to an open object, unload_lib() returns a 
//  non-zero error code.
//
//Parameters:
//  handle: A loaded module reference returned from a previous call to load_lib. 
//
//Return Values:
//    -1:   handle is NULL
//    -2:   library allready unloaded
//************************************************************************
int fake_dll_lib_unload(void *handle)
{
#ifdef _DEBUG
    TTLibraries::iterator iI = std::find_if(libs.begin(), libs.end(), handle_finder(handle));
    assert(iI != libs.end());
#endif
    
    int ret = 0;
    
    if (handle == NULL)
    {
        ret = -1;
    }
    if (ret == 0)
    {
        TTLibraryPair& libPair = *reinterpret_cast<TTLibraryPair*>(handle);
        if (libPair.first <= 0)
        {
            ret = -2;
        }
        else
        {
            libPair.first--;
        }
    }

    return ret;
}

//************************************************************************
//  Looks up the location of a symbol in a module that is loaded with dlopen.
//
//Description:
//  The get_smb function looks up a named symbol exported from a module loaded by 
//  a previous call to the load_lib subroutine. libhndl specifies a value returned 
//  by a previous call to load_lib. name specifies the name of a symbol exported 
//  from the referenced module. The form should be a NULL-terminated string.  
//  If the named symbol is found, smbhndl is set to its address and a zero (0)
//  value is returned. If the named symbol is not found, or if libhndl or name are
//  invalid, smbhndl is set to NULL and a non-zero error code is returned.
//
//Return Values:
//    -1:   handle is NULL
//    -2:   handle is invalid (not registered)
//    -3:   symbol name is NULL
//    -4:   library allready unloaded
//    -5:   undefined symbol
//************************************************************************
int fake_dll_smb_get(void** smbhndl, void *libhndl, const char *name)
{

   int ret = 0;

   *smbhndl = NULL;
   if (libhndl == NULL)
   {
      ret = -1;
   }
   else if (libs.end() == std::find_if(libs.begin(), libs.end(), handle_finder(libhndl)))
   {
      ret = -2;
   }
   else if (name == NULL)
   {
      ret = -3;
   }
   else
   {
      const TTLibraryPair& libPair = *reinterpret_cast<TTLibraryPair*>(libhndl);
      if (libPair.first <= 0)
      {
         ret = -4;
      }
      else
      {
         const TTLibrary& lib = libPair.second;
         TTLibrary::const_iterator eI = lib.find(name);
         if (eI == lib.end())
         {
            ret = -5;
         }
         else
         {
            *smbhndl = (*eI).second;
         }
      }
    }

    return ret;
}

#ifdef __cplusplus
}
#endif
