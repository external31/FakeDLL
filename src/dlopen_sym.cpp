//************************************************************************
//  These libdl stub replaces the operating system functions dlopen(...)
//  and dlsym(...) functions. They are intended for cases where dynamic
//  shared object loaded at runtime are not permitted, or when the shared object
//  should be incorporated into the code to build just one single executable.
//  They allow with very few changes to keep the same software architecture.
//
//  All the symbols from the shared objects must be registered prior use, with
//  a call to the function fake_dll_lib_reg.
//  Then function pointers will be retrieve with dlopen and dlsym calls.
//************************************************************************
#include <assert.h>
#include <algorithm>
#include <string>
#include <map>

typedef std::map<std::string, void*>         TTLibrary;     // One lib
typedef std::pair<int, TTLibrary>            TTLibraryPair; // One lib + counter
typedef std::map<std::string, TTLibraryPair> TTLibraries;   // All the libs
static TTLibraries libs;
static std::string errst;

#ifdef _DEBUG
//  Predicate to control if an handle is known
struct handle_finder
{
    handle_finder(void* v_) : v(v_) {}
    bool operator() (const TTLibraries::value_type& l) const
    {
        return &(l.second) == v;
    }
    void* v;
};
#endif


#ifdef __cplusplus
extern "C"
{
#endif

//************************************************************************
//  The function fake_dll_lib_reg will register a DLL/SO symbol with
//  the supplied parameters.
//
//  Input:
//      void *fn;                   Opaque function pointer
//      const char *symbol;         Symbol name
//      const char *library         Shared Library (DLL) name
//************************************************************************
void fake_dll_lib_reg(void *fn , const char *symbol, const char *library)
{
#ifdef _DEBUG
    assert(fn      != NULL);
    assert(symbol  != NULL);
    assert(library != NULL);
    assert(strlen(symbol)  > 0);
    assert(strlen(library) > 0);
#endif  // _DEBUG

    TTLibraries::iterator libPairI = libs.find(library);
    if (libPairI == libs.end())
    {
        libPairI = libs.insert( TTLibraries::value_type(library, TTLibraryPair()) ).first;
        libPairI->second.first = 0;
    }
    libPairI->second.second[symbol] = fn;
}

//************************************************************************
//  Dynamically load a module into the calling process.
//
//Description:
//  The dlopen subroutine loads the module specified by library. The argument
//  flag is unused. If the module is already loaded, it is not loaded again, but 
//  a reference count will be incremented.
//  The value returned by dlopen may be used in subsequent calls to dlsym and 
//  dlclose. If an error occurs during the operation, dlopen returns NULL.
//
//Return Values:
//  Upon successful completion, dlopen returns a value that can be used in calls 
//  to the dlsym and dlclose functions. If the dlopen call fails, NULL (a value 
//  of 0) is returned. Further information is available via the dlerror function.
//
//************************************************************************
void *dlopen(const char *library, int /*flag*/)
{
    void *retP = NULL;

    TTLibraries::iterator libsI = libs.find(library);
    if (libsI == libs.end())
    {
        errst = std::string("fakedll::dlopen: unknown library: ") + library;
    }
    else
    {
        libsI->second.first++;       // Increment library counter
        retP = &(libsI->second);     // Return a pointer to the pair
    }

    return retP;
}

//************************************************************************
//  Closes and unloads a module loaded by the dlopen function.
//
//Description:
//  The dlclose function is used to remove access to a module loaded with
//  the dlopen function. In addition, access to dependent modules of the
//  module being unloaded is removed as well.
//
//Parameters:
//  handle: A loaded module reference returned from a previous call to dlopen. 
//
//Return Values:
//  Upon successful completion, 0 (zero) is returned. If the object could not be 
//  closed, or if handle does not refer to an open object, dlclose() returns a 
//  non-zero value. More detailed diagnostic information is available 
//  through dlerror().
//************************************************************************
int dlclose(void *handle)
{
#ifdef _DEBUG
    TTLibraries::iterator iI = std::find_if(libs.begin(), libs.end(), handle_finder(handle));
    assert(iI != libs.end());
#endif
    
    int ret = 0;
    
    errst = "";
    if (errst.empty() && handle == NULL)
    {
        errst = "fakedll::dlsym: handle is NULL";
        ret   = -1;
    }
    if (errst.empty())
    {
        TTLibraryPair& libPair = *reinterpret_cast<TTLibraryPair*>(handle);
        if (libPair.first <= 0)
        {
            errst = "fakedll::dlclose: library allready unloaded";
            ret   = -1;
        }
        else
        {
            libPair.first--;
        }
    }

    return ret;
}

//************************************************************************
//  Looks up the location of a symbol in a module that is loaded with dlopen.
//
//Description:
//  The dlsym subroutine looks up a named symbol exported from a module loaded by 
//  a previous call to the dlopen subroutine. handle specifies a value returned 
//  by a previous call to dlopen. name specifies the name of a symbol exported 
//  from the referenced module. The form should be a NULL-terminated string.  
//
//Return Values:
//  If the named symbol is found, its address is returned. If the named symbol is 
//  not found, or if Data or Symbol are invalid, NULL is returned. More detailed 
//  diagnostic information is available through dlerror().
//************************************************************************
void *dlsym(void *handle, const char *name)
{
#ifdef _DEBUG
    TTLibraries::iterator iI = std::find_if(libs.begin(), libs.end(), handle_finder(handle));
    assert(iI != libs.end());
#endif

    errst = "";

    void *sym = NULL;
    if (errst.empty() && handle == NULL)
    {
        errst = "fakedll::dlsym: handle is NULL";
    }
    if (errst.empty() && name == NULL)
    {
        errst = "fakedll::dlsym: symbol name is NULL";
    }
    if (errst.empty())
    {
        const TTLibraryPair& libPair = *reinterpret_cast<TTLibraryPair*>(handle);
        if (libPair.first <= 0)
        {
            errst = "fakedll::dlsym: library allready unloaded";
        }
        else
        {
            const TTLibrary& lib = libPair.second;
            TTLibrary::const_iterator eI = lib.find(name);
            if (eI == lib.end())
            {
                errst = std::string("fakedll::dlsym: undefined symbol: ") + name;
            }
            else
            {
                sym = (*eI).second;
            }
        }
    }

    return sym;
}

//************************************************************************
//  Return a pointer to information about the last dlopen, dlsym, or dlclose error.
//
//Description:
//  The dlerror subroutine is used to obtain information about the last error that 
//  occurred in a dynamic loading routine (that is, dlopen , dlsym , or dlclose). 
//  The returned value is a pointer to a null-terminated string without a final 
//  newline. 
//  Once a call is made to this function, subsequent calls without any intervening 
//  dynamic loading errors will return NULL.
//************************************************************************
char *dlerror()
{
    const char *ret = (errst.empty()) ? NULL : errst.c_str();
//    errst = ""; !!@$#**&?
    return const_cast<char*>(ret);
}

#ifdef __cplusplus
}
#endif
