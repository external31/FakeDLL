//************************************************************************
//  These libdl stub replaces the operating system functions dlopen(...)
//  and dlsym(...) functions. They are intended for cases where dynamic
//  shared object loaded at runtime are not permitted, or when the shared object
//  should be incorporated into the code to build just one single executable.
//  They allow with very few changes to keep the same software architecture.
//
//  All the symbols from the shared objects must be registered prior use, with
//  a call to the function fake_dll_lib_reg.
//  Then function pointers will be retrieve with dlopen and dlsym calls.
//************************************************************************
#include "unxdl.h"

#include "fakedll.h"

#include <algorithm>
#include <ctype.h>      // win64: do not use <cctype>
#include <string>

static std::string errst;

#ifdef __cplusplus
extern "C"
{
#endif

//************************************************************************
//  Dynamically load a module into the calling process.
//
//Description:
//  The dlopen subroutine loads the module specified by library. The argument
//  flag is unused. If the module is already loaded, it is not loaded again, but 
//  a reference count will be incremented.
//  The value returned by dlopen may be used in subsequent calls to dlsym and 
//  dlclose. If an error occurs during the operation, dlopen returns NULL.
//
//Return Values:
//  Upon successful completion, dlopen returns a value that can be used in calls 
//  to the dlsym and dlclose functions. If the dlopen call fails, NULL (a value 
//  of 0) is returned. Further information is available via the dlerror function.
//
//************************************************************************
void *FAKE_DLL_dlopen(const char *library, int /*flag*/)
{
   std::string lib(library);
   if (lib.size() > 3)
   {
      std::string ext = lib.substr(lib.size()-3);
      std::transform(ext.begin(), ext.end(), ext.begin(), tolower);
      if (ext == ".so")
         lib = lib.substr(0, lib.size()-3);
   }
   if (lib.size() > 3)
   {
      std::string pre = lib.substr(0, 3);
      if (pre == "lib")
         lib = lib.substr(3);
   }

   void *handle = NULL;
   int ret = fake_dll_lib_load(&handle, lib.c_str());
   
   errst = "";
   switch (ret)
   {
      case -1:
         errst = std::string("fakedll::dlopen: unknown library: ") + library;
         break;
   }

   return handle;
}

//************************************************************************
//  Closes and unloads a module loaded by the dlopen function.
//
//Description:
//  The dlclose function is used to remove access to a module loaded with
//  the dlopen function. In addition, access to dependent modules of the
//  module being unloaded is removed as well.
//
//Parameters:
//  handle: A loaded module reference returned from a previous call to dlopen. 
//
//Return Values:
//  Upon successful completion, 0 (zero) is returned. If the object could not be 
//  closed, or if handle does not refer to an open object, dlclose() returns a 
//  non-zero value. More detailed diagnostic information is available 
//  through dlerror().
//************************************************************************
int FAKE_DLL_dlclose(void *handle)
{
   int ret = fake_dll_lib_unload(handle);
    
   errst = "";
   switch (ret)
   {
      case -1:
         errst = "fakedll::dlsym: handle is NULL";
         break;
      case -2:
         errst = "fakedll::dlclose: library allready unloaded";
         break;
   }
   
   return ret;
}

//************************************************************************
//  Looks up the location of a symbol in a module that is loaded with dlopen.
//
//Description:
//  The dlsym subroutine looks up a named symbol exported from a module loaded by 
//  a previous call to the dlopen subroutine. handle specifies a value returned 
//  by a previous call to dlopen. name specifies the name of a symbol exported 
//  from the referenced module. The form should be a NULL-terminated string.  
//
//Return Values:
//  If the named symbol is found, its address is returned. If the named symbol is 
//  not found, or if Data or Symbol are invalid, NULL is returned. More detailed 
//  diagnostic information is available through dlerror().
//************************************************************************
void *FAKE_DLL_dlsym(void *handle, const char *name)
{
   void* smbhndl = NULL;
   int ret = fake_dll_smb_get(&smbhndl, handle, name);

   errst = "";
   switch (ret)
   {
      case -1:
         errst = "fakedll::dlsym: handle is NULL";
         break;
      case -2:
         errst = "fakedll::dlsym: handle is not registered";
         break;
      case -3:
         errst = "fakedll::dlsym: symbol name is NULL";
         break;
      case -4:
         errst = "fakedll::dlsym: library allready unloaded";
         break;
      case -5:
         errst = std::string("fakedll::dlsym: undefined symbol: ") + name;
         break;
    }

    return smbhndl;
}

//************************************************************************
//  Return a pointer to information about the last dlopen, dlsym, or dlclose error.
//
//Description:
//  The dlerror subroutine is used to obtain information about the last error that 
//  occurred in a dynamic loading routine (that is, dlopen , dlsym , or dlclose). 
//  The returned value is a pointer to a null-terminated string without a final 
//  newline. 
//  Once a call is made to this function, subsequent calls without any intervening 
//  dynamic loading errors will return NULL.
//************************************************************************
char *FAKE_DLL_dlerror()
{
    const char *ret = (errst.empty()) ? NULL : errst.c_str();
//    errst = ""; !!@$#**&?
    return const_cast<char*>(ret);
}

#ifdef __cplusplus
}
#endif
