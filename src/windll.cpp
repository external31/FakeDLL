//************************************************************************
//  These windll stub replaces the operating system functions LoadLibrary(...)
//  and dlsym(...) functions. They are intended for cases where dynamic
//  shared object loaded at runtime are not permitted, or when the shared object
//  should be incorporated into the code to build just one single executable.
//  They allow with very few changes to keep the same software architecture.
//
//  All the symbols from the shared objects must be registered prior use, with
//  a call to the function fake_dll_lib_reg.
//************************************************************************
#ifndef FAKE_DLL
#  define FAKE_DLL
#endif

#include "windll.h"
#include "fakedll.h"

#include <algorithm>
#include <ctype.h>      // win64: do not use <cctype>
#include <string>

#ifdef __cplusplus
extern "C"
{
#endif

//************************************************************************
//  Dynamically load a module into the calling process.
//
//Description:
//  The dlopen subroutine loads the module specified by library. The argument
//  flag is unused. If the module is already loaded, it is not loaded again, but 
//  a reference count will be incremented.
//  The value returned by dlopen may be used in subsequent calls to dlsym and 
//  dlclose. If an error occurs during the operation, dlopen returns NULL.
//
//Return Values:
//  Upon successful completion, dlopen returns a value that can be used in calls 
//  to the dlsym and dlclose functions. If the dlopen call fails, NULL (a value 
//  of 0) is returned. Further information is available via the dlerror function.
//
//************************************************************************
HMODULE WINAPI FAKE_DLL_LoadLibrary(LPCTSTR library)
{
   std::string lib(library);
   if (lib.size() > 4)
   {
      std::string ext = lib.substr(lib.size()-4);
      std::transform(ext.begin(), ext.end(), ext.begin(), tolower);
      if (ext == ".dll")
         lib = lib.substr(0, lib.size()-4);
   }
   
   void *handle = NULL;
   int ret = fake_dll_lib_load(&handle, lib.c_str());
   
   switch (ret)
   {
      case -1:
         SetLastError(126);     // Module not found
         break;
   }

   return HMODULE(handle);
}

//************************************************************************
//  Closes and unloads a module loaded by the dlopen function.
//
//Description:
//  The dlclose function is used to remove access to a module loaded with
//  the dlopen function. In addition, access to dependent modules of the
//  module being unloaded is removed as well.
//
//Parameters:
//  handle: A loaded module reference returned from a previous call to dlopen. 
//
//Return Values:
//  Upon successful completion, 0 (zero) is returned. If the object could not be 
//  closed, or if handle does not refer to an open object, dlclose() returns a 
//  non-zero value. More detailed diagnostic information is available 
//  through dlerror().
//************************************************************************
BOOL WINAPI FAKE_DLL_FreeLibrary(HMODULE handle)
{
   
   int ret = fake_dll_lib_unload(handle);
    
   switch (ret)
   {
      case -1:
         SetLastError(6);     // Invalid handle
         break;
      case -2:
         SetLastError(126);   // Module not found
         break;
   }
   
   return (ret == 0);
}

//************************************************************************
//  Looks up the location of a symbol in a module that is loaded with dlopen.
//
//Description:
//  The dlsym subroutine looks up a named symbol exported from a module loaded by 
//  a previous call to the dlopen subroutine. handle specifies a value returned 
//  by a previous call to dlopen. name specifies the name of a symbol exported 
//  from the referenced module. The form should be a NULL-terminated string.  
//
//Return Values:
//  If the named symbol is found, its address is returned. If the named symbol is 
//  not found, or if Data or Symbol are invalid, NULL is returned. More detailed 
//  diagnostic information is available through dlerror().
//************************************************************************
FARPROC WINAPI FAKE_DLL_GetProcAddress(HMODULE handle, LPCSTR name)
{
   void* smbhndl = NULL;
   int ret = fake_dll_smb_get(&smbhndl, handle, name);

   switch (ret)
   {
      case -1:
         SetLastError(6);     // Invalid handle
         break;
      case -2:
         SetLastError(6);     // Invalid handle
         break;
      case -3:
         SetLastError(87);    // Invalid parameter
         break;
      case -4:
         SetLastError(126);   // Module not found
         break;
      case -5:
         SetLastError(127);   // Procedure not found
         break;
    }

    return FARPROC(smbhndl);
}

#ifdef __cplusplus
}
#endif
