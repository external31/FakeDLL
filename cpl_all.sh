#!/bin/bash

./cpl_one.sh gcc         openmpi-1.4.5
./cpl_one.sh itlX64-14.0 openmpi-1.4.5
./cpl_one.sh itlX86-14.0 openmpi-1.4.5
./cpl_one.sh sun-12.3    openmpi-1.4.5
./cpl_one.sh open64-5.0  openmpi-1.4.5

./cpl_one.sh gcc         openmpi-1.6
./cpl_one.sh itlX64-14.0 openmpi-1.6
./cpl_one.sh itlX86-14.0 openmpi-1.6
./cpl_one.sh sun-12.3    openmpi-1.6
./cpl_one.sh open64-5.0  openmpi-1.6

./cpl_one.sh gcc         openmpi-1.6 i8
./cpl_one.sh itlX64-14.0 openmpi-1.6 i8
#./cpl_one.sh sun-12.3    openmpi-1.6 i8
./cpl_one.sh open64-5.0  openmpi-1.6 i8

