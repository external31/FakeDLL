#include <stdlib.h>
#include <string.h>
#include <malloc.h>

typedef struct{   /* symbol table entry */
 char *name;      /* symbol name */
 void *fn;        /* symbol address */
}SYMTABENTRY;

#define SYMPAGE 128
typedef struct PaGe{    /* symbol table page */
 struct PaGe *next;     /* pointer to next page */
 int nsym;              /* index for next free entry */
 SYMTABENTRY s[SYMPAGE];
}PAGE;

typedef struct{         /* library entry */
 char *name;            /* library name */
 PAGE *first;           /* pointer to first page of symbol table fot this library */
}LIBENTRY;

#define MAXLIBS 128
static int nlibs=0;     /* number of libraries */
static LIBENTRY libs[MAXLIBS];  /* library table */

/* "dlopen"(const char *pathname, int mode) */
/*  find library entry associated to name, create it if it does not exist */
LIBENTRY *find_lib_in_table(char *library, int dummy)
{
  int i;

PRINTF("looking for address of %s\n",library);
  for (i=0 ; i<nlibs ; i++) { if(libs[i].name==library) {
PRINTF("found at position %d\n",i);
    return(&libs[i]); };  /* found it */
  }

  if(nlibs>=MAXLIBS) return(NULL);                     /* OOPS, table is full */
  libs[nlibs].name=library;
  libs[nlibs].first=(PAGE *)calloc(1,sizeof(PAGE));    /* allocate first page */
  libs[nlibs].first->next=NULL;
PRINTF("created library entry %s at position %d \n",libs[nlibs].name,nlibs);
  nlibs++;
  return(&libs[nlibs-1]);                              /* return pointer to library entry */
}

/* add symbol to a library (symbol goes into first page) */
void add_symbol_to_lib(LIBENTRY *lib,char *name,void *fn)
{
  PAGE *page=lib->first;
  if(page->nsym >= SYMPAGE) {                         /* page is full, add one more */
    PAGE *temp=(PAGE *)calloc(1,sizeof(PAGE));
    temp->next=lib->first;
    lib->first=temp;
  }else{                                              /* fill free entry, bump index to free entry */
    page->s[page->nsym].fn=fn;
    page->s[page->nsym].name=name;
PRINTF("added %s as entry no %d \n",page->s[page->nsym].name,page->nsym);
    page->nsym++;
  }
}

void *find_symbol_in_page(PAGE *page,char *name)
{
  int i;
PRINTF("looking for symbol %s, there are %d symbols in page\n",name,page->nsym);
  for ( i=0 ; i<page->nsym ; i++ ) {
PRINTF("trying match against %s\n",page->s[i].name);
    if( strcmp(name,page->s[i].name) == 0 ){
PRINTF("found at position %d\n",i);
      return(page->s[i].fn);
    }
  }
  return(NULL) ;  /* failed to find anything */
}

/* "dlsym"(void *handle, const char *name)  */
void *find_symbol_in_lib(LIBENTRY *lib,char *name) /* find symbol address in symbol table */
{
  PAGE *page=lib->first;
  void *target = find_symbol_in_page(page,name);
  if(target != NULL) return(target);
  while(page->next != NULL){
    page = page->next;
    target = find_symbol_in_page(page,name);
    if(target != NULL) return(target);
  }
  return(NULL);
}

void pseudo_dll_lib_prep(void *fn , char *symbol, char *library)
{
  LIBENTRY *lib=find_lib_in_table(library,0);
  add_symbol_to_lib(lib,symbol,fn);
}
#ifdef FAKE_DLL
void *dlopen(char *filename, int flag)
{
  return( find_lib_in_table(filename,flag) );
}
void *dlsym(void *handle, const char *name)
{
return ( find_symbol_in_lib(handle,name) );
}
#endif
