#include <stdio.h>
#include <dlfnc.h>

static char *table_name2="zoinzoin2";
static char *table_name="zoinzoin";

extern "C" void fake_dll_lib_reg(void* , const char *, const char *);

char *pseudo_dll_libzoinzoin() {
{ extern void (*zarza)(); static char *zarza_name="zarza" ; fake_dll_lib_reg(&zarza,zarza_name,table_name) ; }
{ extern void (*zozo)(); static char *zozo_name="zozo" ; fake_dll_lib_reg(&zozo,zozo_name,table_name) ; }
return(table_name); }
char *pseudo_dll_libzoinzoin2() {
{ extern void (*zarza2)(); static char *zarza_name="zarza2" ; fake_dll_lib_reg(&zarza2,zarza_name,table_name2) ; }
{ extern void (*zozo2)(); static char *zozo_name="zozo2" ; fake_dll_lib_reg(&zozo2,zozo_name,table_name2) ; }
return(table_name); }

void pseudo_dlls()
{
pseudo_dll_libzoinzoin();
pseudo_dll_libzoinzoin2();
}

main()
{
   typedef void (*func_t)();

   void *handle, *handle2;
   func_t fn;

   pseudo_dlls();

   handle = dlopen("zoinzoin",0);
   if(handle==NULL) printf("handle is NULL\n");
   handle2=dlopen("zoinzoin2",0);
   if(handle2==NULL) printf("handle2 is NULL\n");
   fn = (func_t) dlsym(handle,"zarza");
   if(fn==NULL) printf("zarza is NULL\n");
   (*fn)(); 
   fn = (func_t) dlsym(handle,"zozo");
   (*fn)(); 
   fn = (func_t) dlsym(handle2,"zarza2");
   (*fn)(); 
   fn = (func_t) dlsym(handle2,"zozo2");
   if(fn==NULL) printf("fn is NULL\n");
   (*fn)(); 
}
